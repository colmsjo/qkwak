//
// Controller logic
// =====================

// Some global variables
// ---------------------

var language = "se",
    currentPage;

var HOME  = "home",
    ABOUT = "about",
    TOS   = "tos";


// Crossroads routing
// ------------------

function setupRouting() {

  // setup crossroads, using the global object. 
  // It is also possible to have several independent Routers
  crossroads.addRoute('', function() { 
    currentPage = HOME;
    showHeader(currentPage);
    showExchange();
    showFooter();
  });

  crossroads.addRoute('about', function() {
    currentPage = ABOUT;
    showHeader(currentPage);
    showAbout();
  }); 

  crossroads.addRoute('tos', function() {
    currentPage = TOS;
    showHeader(currentPage);
    showTOS();
  }); 

  // Also log routes that did not match anything (useful for debugging)
  crossroads.bypassed.add(function(request){
    console.log("A route that wasn't matched:"+request); 
  });

  // setup hasher
  function parseHash(newHash, oldHash) {
    crossroads.parse(newHash); 
  }

  // parse initial hash
  hasher.initialized.add(parseHash);
  // parse hash changes 
  hasher.changed.add(parseHash);
  // start listening for history change
  hasher.init();

  //update URL fragment generating new history record 
  hasher.setHash('');

}


// Setup Google Analytics
// ----------------------

function setupAnalytics() {
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-46757112-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
}


// Show Mt. Gox Ticker
// ----------------------

function getTicker(){
  var request = $.ajax({

    url: 'https://data.mtgox.com/api/2/BTCSEK/money/ticker',
    type: 'GET',

    success: function(data){
        $('#ticker').text(data.data.vwap.display_short);
    },

    error: function(data){
        console.log('mtgox-ticker: Shit hit the fan...' + JSON.stringify(data));
    }

  });
};


// Get language settings 
// ---------------------

function getLang() {
  // works IE/SAFARI/CHROME/FF
  return document.getElementById('lang').innerHTML = (window.navigator.userLanguage || window.navigator.language);
}


// Main
// ----

window.onload = function(){
  setupAnalytics();
  setupRouting();

  //setupExample();
  //showHeader();
  //showExchange();
  //showFooter();
}


