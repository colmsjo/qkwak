/**
 * @jsx React.DOM
 */


//
// Views
// =====================

// Example react app
// -----------------

var Application = React.createClass({
  render: function() {
    var elapsed = Math.round(this.props.elapsed  / 100);
    var seconds = elapsed / 10 + (elapsed % 10 ? '' : '.0' );
    var message =
      'React has been successfully running for ' + seconds + ' seconds.';

    return <p>{message}</p>;
  }
});

var start = new Date().getTime();

function setupExample() {
  setInterval(function() {
    React.renderComponent(
      <Application elapsed={new Date().getTime() - start} />,
      document.getElementById('myfooter')
    );
  }, 50);
}



// Header
// -------
// TODO: fix   <li class="active"><a href="index.html">Växla</a></li>

var HeaderView = React.createClass({
  render: function() {
    var message = [];

    message[HOME]  = "Växla bitcoins smidigt och enkelt";
    message[ABOUT] = "Allt du behöver för att köpa bitcoins är en bitcoin-plånbok och ett kreditkort!";

    var jumbotron = <div className="jumbotron"><h2>{message[this.props.page]}</h2></div>;

    if(this.props.page === TOS) jumbotron="";

    return  <div className="header">
              <ul className="nav nav-pills pull-right">
                <li><a href="#">Växla</a></li>
                <li><a href="#about">Om</a></li>
              </ul>
              <h3 className="text-muted">Qkwak - &#123; kwæk  &#125;</h3>
              {jumbotron}
            </div>;

/* Remove Mt. Gox quote return  <div className="header">
              <ul className="nav nav-pills pull-right">
                <li><a href="#">Växla</a></li>
                <li><a href="#about">Om</a></li>
              </ul>
              <h3 className="text-muted">Qkwak - &#123; kwæk  &#125; <br/>
                <small>Aktuell kurs: </small><small id="ticker"></small><small>/BTC</small>
              </h3>
              {jumbotron}
            </div>;
*/
  }
});

function showHeader(page) {
  React.renderComponent(
    <HeaderView page={page}/>,
    document.getElementById('myheader')
  );

  getTicker();

};


// Footer 
// -------

var FooterView = React.createClass({
  render: function() {
    return <div className="footer">
            <div className="row">
              <div className="col-md-12">
                <small>
                  &copy; Gizur AB 2013 
                  | <a href="mailto:info@gizur.com">info@gizur.com</a>
                  | Munkerödsvägen 4A, 44432 Stenungsund, SVERIGE
                  | org. nr. 556858-6282
                  | <a href="#tos">Användaravtal</a>
                </small>
              </div>
            </div>
          </div>;

  }
});

function showFooter() {
  React.renderComponent(
    <FooterView />,
    document.getElementById('myfooter')
  );
};



// Exchange 
// -------

var ExchangeView = React.createClass({
  render: function() {
    return  <div className="row marketing">
              <div className="col-md-12">

               <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">

                  <div className="row">
                    <div className="col-md-4">
                      <input type="hidden" name="cmd" value="_s-xclick"/>
                      <input type="hidden" name="hosted_button_id" value="P5W5UMLL4FX6S"/>
                      <input type="hidden" name="on0" value="Växla SEK till bitcoins">Växla SEK till bitcoins</input>
                    </div>

                    <div className="col-md-8">
                      <select name="os0">
                          <option value="Hundra kronor  -">Hundra kronor  - 100,00 SEK</option>
                          <option value="Tusen kronor -">Tusen kronor - 1 000,00 SEK</option>
                          <option value="Tiotusen kronor -">Tiotusen kronor - 10 000,00 SEK</option>
                      </select>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-12">
                      <p></p>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-4">
                      <input type="hidden" name="on1" value="Bitcoin adress">Bitcoin address</input>
                    </div>

                    <div className="col-md-8">
                      <input type="text" name="os1" maxLength="200"/>
                      <input type="hidden" name="currency_code" value="SEK"/>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-12">
                      <p></p>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-2">
                      <input type="image" src="https://www.paypalobjects.com/en_US/SE/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"/>
                    </div>
                    <div className="col-md-6"></div>
                  </div>
                  
                  <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-8">
                      <small className="text-muted">All data skickas över en säker anslutning</small>
                    </div>
                  </div>


              </form>

              </div>
            </div>;
  }
});



function showExchange() {
  React.renderComponent(
    <ExchangeView />,
    document.getElementById('mycontent')
  );
};


// About
// -----

var AboutView = React.createClass({
  render: function() {
    return  <div className="row marketing">
              <div className="col-md-12">

                <p className="text-info">Så här går det till:</p>
                <ol className="text-info">
                  <li>Välj en bitcoin-plånbok om du inte redan har en. På <a href="http://www.bitcoin.se/planbocker/">bitcoin.se</a> finns lite tips.</li>
                  <li>Skapa en bitcoin address i din plånbok</li>
                  <li>Mata in bitcoin-addressen, välj hur mycket du vill växla och klicka på <em>Buy Now</em> på <a href="#">växlingssidan</a></li>
                  <li>Välj <em>Har du inget PayPal-konto?</em> och mata in dina kreditkorts- och kontaktuppgifter och välj <em>Betala</em></li>
                  <li>Dina bitcoins kommer att finnas i din bitcoin-plånbok inom någon timma</li>
                  <li>Du får den kurs som gäller när växlingen genomförs och avgiften är 5% utöver PayPals avgift</li>
                </ol>

              </div>
            </div>;

  }
});

function showAbout() {
  React.renderComponent(
    <AboutView />,
    document.getElementById('mycontent')
  );
};


// Terms Of Servie (TOS)
// ---------------------

var TOSView = React.createClass({
  render: function() {
    return  <div className="row marketing">
              <div className="col-md-12">

                <p className="text-primary">
                Qkwak kan inte garanterar oavbruten tillgång till växlingstjänsten eftersom
                tjänsten kan störas av många faktorer utanför vår kontroll. Qkwak tillhandahålls 
                utan garanti och i den mån lagen tillåter friskriver vi oss från alla garantier eller 
                andra förpliktelser, både lagliga och övriga, inklusive, men inte  begränsat till, 
                krav avseende prestation på viss tid. Användaren åtar sig att hålla Qkwak och dess 
                ägare skadelöst.</p>

                <p className="text-primary">
                Växlingstjänsten får inte användas för att bryta mot svensk lag eller internationell rätt. 
                Qkwak friskriver sig från allt ansvar som härstammar från tredje part.</p>

                <p className="text-primary">
                Eftersom alla betalning görs med PayPal gäller PayPals användaravtal för alla 
                betalningar.</p>

                <p className="text-primary">
                Qkwak använder inte cookies. See PayPals användarvillkor angående deras användande av cookies.</p>

                <p className="text-primary">
                Qkwak sparar inga personuppgifter. Qkwak ansvarar dock inte för PayPals eventuella
                användning av personuppgifter. </p>

                <p className="text-primary">
                Qkwak förbehåller sig rätten att ändra dessa villkor. Eftersom vi inte sparar några 
                användaruppgifter kan vi inte meddela sig som kund.</p>

                <p className="text-primary">
                Svensk lag gäller för användaravtalet inklusive dessa villkor. Tvister avseende 
                giltighet, tolkning, tillämplighet eller tillämpning av användaravtalet och dessa 
                villkor, samt rättsförhållanden som uppstått härav, skall avgöras av allmän domstol 
                varvid Göteborgs tingsrätt skall vara första instans.</p>

              </div>
            </div>;

  }
});

function showTOS() {
  React.renderComponent(
    <TOSView />,
    document.getElementById('mycontent')
  );
};


