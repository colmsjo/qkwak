Qkwak
=====

This is a simple static HTML page with a PayPal button. It using the Twitter Bootstrap 
theme. Install the theme using  `bower install`. Bower is installed with `npm install -g bower`.

The site is published using Amazon on this adress: http://qkwak.s3-website-eu-west-1.amazonaws.com/#

Howto setup S3 buckets for web-sites is described here:
Publish site using Amazon S3: http://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html


Bucket policy:

```
{
  "Version":"2012-10-17",
  "Statement":[{
	"Sid":"AddPerm",
        "Effect":"Allow",
	  "Principal": {
            "AWS": "*"
         },
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::qkwak.com/*"
      ]
    }
  ]
}
```

Build site (pre-compile the JSX)
-------------------------------

This is not necessary during development but improves performance when deploying to production.

`cd js && jsx . ../dist/js/`


Run site on local web server
---------------------------

Steps:

 1. `npm install`
 1. `./http-server.js`

Open http://localhost:[port]


Create mail account (needed for SSL)
-----------------------------------

RapidSSL will send the certificate to a mail adress in the domain registered.

Follow the instructions at http://www.google.com/intx/en/enterprise/apps/business/index.html


SSL setup for AWS
----------------

Generate CSR, see
https://knowledge.rapidssl.com/support/ssl-certificate-support/index?page=content&actp=CROSSLINK&id=SO21323

```
# The password used for cloud@gizur.com is used here as well
openssl genrsa -des3 -out qkwak.key 2048

openssl req -new -key qkwak.key -out qkwak.csr 
```

Apply for a certificate at RapidSSL.com



Improve google search results
-----------------------------

I've done the following

 1. Setup Google Analytics code
 1. Setup Alexa
 1. Added to dmoz directory

Tips:

 * Sitelinks - http://www.hochmanconsultants.com/articles/sitelinks.shtml








