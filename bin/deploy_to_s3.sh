#!/bin/bash

CMD="s3cmd -c s3cfg "
BUCKET="s3://qkwak.com"

echo "Files in bucket before deployment"
$CMD ls $BUCKET
$CMD ls $BUCKET/js/
$CMD ls $BUCKET/css/
$CMD ls $BUCKET/bower_compenents/

$CMD put sitemap.xml  $BUCKET
$CMD put robots.txt   $BUCKET
$CMD put *.html       $BUCKET
$CMD sync js $BUCKET/
$CMD sync css $BUCKET/
$CMD sync bower_components $BUCKET/


echo "Files in bucket after deployment"
$CMD ls $BUCKET
$CMD ls $BUCKET/js/
$CMD ls $BUCKET/css/
$CMD ls $BUCKET/bower_compenents/
